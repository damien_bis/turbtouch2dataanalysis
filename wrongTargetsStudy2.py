import numpy as np
import pandas as pd
import math
import os
import sys

dwellErrors = 0
fsrErrors = 0
doubleTapErrors = 0


def compute(filename):
    global dwellErrors
    global fsrErrors
    global doubleTapErrors

    df = pd.read_csv(filename, sep=';')

    df = df[df["TouchEvent"] == "BUTTON_TRIGGERED"]
    df = df[df["WidgetHighlighted"] != df["WidgetHovered"]]

    if "dwell" in filename:
        dwellErrors = dwellErrors + len(df)
    elif "fsr" in filename:
        fsrErrors = fsrErrors + len(df)
    else:
        doubleTapErrors = doubleTapErrors + len(df)

    print filename, len(df)

def compute_in_dir(dirname):
    for f in os.listdir(dirname):
        if ((len(sys.argv) == 1 or sys.argv[1] in f) and "study2" in f):
            compute(dirname + f)


root = "logs/"
for p in os.listdir(root):
    path = root + p + "/"
    compute_in_dir(path)


print "Dwell : ", dwellErrors
print "FSR :", fsrErrors
print "Double tap :", doubleTapErrors

