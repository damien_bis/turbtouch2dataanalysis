import numpy as np
import pandas as pd
import math
import os
import sys

def compute(filename):
    df = pd.read_csv(filename, sep=';')

    df = df[df["TouchEvent"] == "BUTTON_TRIGGERED"]

    for idx, row in df.iterrows():
        nb_touch = df.get_value(idx, "TouchCount")
        subject = str(df.get_value(idx, "Participant"))
        method = df.get_value(idx, "Method")
        brace = "1" if nb_touch > 1 else "0"
        vibration = "VIBRATION" if "vibration" in filename else "STATIC"
        print "\t".join([subject, method, vibration, brace])


def compute_in_dir(dirname):
    for f in os.listdir(dirname):
        if ((len(sys.argv) == 1 or sys.argv[1] in f) and "study2" in f):
            compute(dirname + f)


root = "logs/"
for p in os.listdir(root):
    path = root + p + "/"
    compute_in_dir(path)
