import numpy as np
import pandas as pd
import math
import os
import sys

dwellErrors = 0
fsrErrors = 0
doubleTapErrors = 0

class Touch:
    def __init__(self, tid):
        self.touch_id = tid
        self.row_idx = []
        self.released = False
        self.trigg_btn = False
        self.trigg_btn_idx = 0

    def addRow(self, rowId):
        self.row_idx.append(rowId)


def get_touch(touches, tid):
    idx = len(touches) - 1
    while (idx >= 0):
        if (touches[idx].touch_id == tid):
            return touches[idx]
        idx = idx - 1
    return None


def compute(filename):
    global dwellErrors
    global fsrErrors
    global doubleTapErrors


    df = pd.read_csv(filename, sep=';')

    touches = []
    btn_touches = []
    # Create list of all the touches that happened
    for idx, row in df.iterrows():
        ev = row["TouchEvent"]
        if (ev == "TOUCH_PRESSED"):
            t = Touch(row["TouchId"])
            t.addRow(idx)
            touches.append(t)
        elif (ev == "TOUCH_MOVED"):
            get_touch(touches, row["TouchId"]).addRow(idx)
        elif (ev == "TOUCH_RELEASED"):
            t = get_touch(touches, row["TouchId"])
            t.addRow(idx)
            t.released = True
        elif (ev == "BUTTON_TRIGGERED"):
            t = get_touch(touches, row["TouchId"])
            t.trigg_btn = True
            t.trigg_btn_idx = idx
            btn_touches.append(t)

    touch_press = df[df["TouchEvent"] == "TOUCH_PRESSED"]

    # Loop through touches that triggered a button
    for t in btn_touches:
        triggidx = t.trigg_btn_idx

        if (df.get_value(triggidx, "WidgetHovered") != df.get_value(triggidx, "WidgetHighlighted")):
            pressidx = t.row_idx[0]
            timestamp = df.get_value(pressidx, "Timestamp")

            dwell_touch = touch_press[touch_press["Timestamp"] > timestamp]
            dwell_touch = dwell_touch[dwell_touch["Timestamp"] <= timestamp + 500]
            if (len(dwell_touch) == 0):
                dwellErrors = dwellErrors + 1

    print filename


def compute_in_dir(dirname):
    for f in os.listdir(dirname):
        if ((len(sys.argv) == 1 or sys.argv[1] in f) and "study2" in f and "dwell" in f):
            compute(dirname + f)


root = "logs/"
for p in os.listdir(root):
    path = root + p + "/"
    compute_in_dir(path)


print "Dwell : ", dwellErrors
print "FSR :", fsrErrors
print "Double tap :", doubleTapErrors

