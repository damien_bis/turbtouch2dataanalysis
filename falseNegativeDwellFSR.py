import numpy as np
import pandas as pd
import math
import os
import sys
class Touch:
    def __init__(self, tid):
        self.touch_id = tid
        self.row_idx = []
        self.released = False
        self.trigg_btn = False
        self.trigg_btn_idx = 0

    def addRow(self, rowId):
        self.row_idx.append(rowId)


class Trial:
    def __init__(self, widgetName, targetSize):
        self.touches = []
        self.widgetName = widgetName
        self.targetSize = targetSize

def get_touch(touches, tid):
    idx = len(touches) - 1
    while (idx >= 0):
        if (touches[idx].touch_id == tid):
            return touches[idx]
        idx = idx - 1
    return None


hitboxes = pd.read_csv("hitbox.csv", sep=';')

# Get the hitbox of the highlighted target specified in a row
def get_hitbox_from_row(df, rowid):
    widget = df.get_value(rowid, "WidgetHighlighted")
    radius = df.get_value(rowid, "TargetRadius")

    h = hitboxes[hitboxes["Study"] == df.get_value(rowid, "Study")]
    width = 81.0
    if (radius == 33.0):
        width = 99.0
    elif (radius == 27.0):
        width = 81.0
    elif (radius == 20.0):
        width = 60.0

    h = h[h["Width"] == width]
    return h[h["Widget"] == widget].iloc[0]

# Compute the (euclidean) distance between two points
def dist(ax, ay, bx, by):
    dx = ax - bx
    dy = ay - by
    dist = dx * dx + dy * dy
    if dist > 0:
        dist = math.sqrt(dist)

    return dist

# Return true if TOUCH_PRESS and TOUCH_RELEASE are within 50px of the highlighted target
def is_touch_mistake(df, t):
    press = t.row_idx[0]
    release = t.row_idx[-1]
    target = get_hitbox_from_row(df, press)
    targetRadius = df.get_value(press, "TargetRadius")

    # Compute distance of press and release from highlighted target
    tx = target["X"] + targetRadius
    ty = target["Y"] + targetRadius
    distPress = dist(tx, ty, df.get_value(press, "TouchX"), df.get_value(press, "TouchY"))
    distRelease = dist(tx, ty, df.get_value(release, "TouchX"), df.get_value(release, "TouchY"))

    if (distPress < targetRadius + 50 and distRelease < targetRadius + 50):
        return True

    return False

# Compute the results for all the trials of a specific file (study1 and study2)
def compute(filename):
    df = pd.read_csv(filename, sep=';')

    # Only interested in touches that could trigger a button (so the first touch when unbraced, and the fifth touch when braced)
    nbFinger = (5 if ("fiveFinger" in filename) else 1)

    if ("study1" in filename):
        df[df["TouchCount"] == nbFinger]

    touches = []
    btn_touches = []
    useless_touches = []
    trials = []
    lastTrial = Trial("None", 0)
    # Create list of all the touches that happened
    for idx, row in df.iterrows():
        ev = row["TouchEvent"]
        if (ev == "TOUCH_PRESSED"):
            t = Touch(row["TouchId"])
            t.addRow(idx)
            touches.append(t)
            widget = df.get_value(idx, "WidgetHighlighted")
            targetSize = df.get_value(idx, "TargetRadius")
            if (widget != "None"):
                if (lastTrial.widgetName != widget or lastTrial.targetSize != targetSize):
                    lastTrial = Trial(widget, targetSize)
                    trials.append(lastTrial)

                lastTrial.touches.append(t)

        elif (ev == "TOUCH_MOVED"):
            get_touch(touches, row["TouchId"]).addRow(idx)
        elif (ev == "TOUCH_RELEASED"):
            t = get_touch(touches, row["TouchId"])
            t.addRow(idx)
            t.released = True
            if (t.trigg_btn_idx == 0):
                useless_touches.append(t)
        elif (ev == "BUTTON_TRIGGERED"):
            t = get_touch(touches, row["TouchId"])
            t.trigg_btn = True
            t.trigg_btn_idx = idx
            btn_touches.append(t)


    # Loop through the trials
    subject = str(df["Participant"].iloc[0])
    method = str(df["Method"].iloc[0])
    brace = "unbraced" if df["Variant"].iloc[0] == "SingleFinger" else "braced"
    vibration = "vibration" if "vibration" in filename else "no_vibration"

    for trial in trials:
        nbError = 0
        # Loop through the touches that didn't trigger a button during the trial
        for t in trial.touches:
            if not t.trigg_btn and is_touch_mistake(df, t):
                nbError = nbError + 1

        size = str(trial.targetSize)
        countMisses = str(nbError)
        if "study2" in filename:
            print '\t'.join([subject, method, vibration, countMisses])
        else:
            print '\t'.join([subject, method, brace, vibration, size, countMisses])


def compute_in_dir(dirname):
    for f in os.listdir(dirname):
        if (not "~" in f and ("fsr" in f or "dwell" in f) and (len(sys.argv) == 1 or sys.argv[1] in f)):
            compute(dirname + f)


root = "logs/"
for p in os.listdir(root):
    path = root + p + "/"
    compute_in_dir(path)
