import numpy as np
import pandas as pd
import math
import os
import sys

hitboxes = pd.read_csv("hitbox.csv", sep=';')

def get_hitbox_from_row(df, rowid):
    widget = df.get_value(rowid, "WidgetHighlighted")
    radius = df.get_value(rowid, "TargetRadius")

    h = hitboxes[hitboxes["Study"] == df.get_value(rowid, "Study")]
    width = 81.0
    if (radius == 33.0):
        width = 99.0
    elif (radius == 27.0):
        width = 81.0
    elif (radius == 20.0):
        width = 60.0

    h = h[h["Width"] == width]
    return h[h["Widget"] == widget].iloc[0]


def dist(ax, ay, bx, by):
    dx = ax - bx
    dy = ay - by
    dist = dx * dx + dy * dy
    if dist > 0:
        dist = math.sqrt(dist)

    return dist



def is_doubletap_mistake(df, row1, row2):
    # Same target highlighted for both
    if (df.get_value(row1, "WidgetHighlighted") != df.get_value(row2, "WidgetHighlighted")):
        return False

    targetRadius = df.get_value(row1, "TargetRadius")

    # Compute distance from highlighted target
    target = get_hitbox_from_row(df, row1)
    tx = target["X"] + targetRadius
    ty = target["Y"] + targetRadius
    dist1 = dist(tx, ty, df.get_value(row1, "TouchX"), df.get_value(row1, "TouchY"))
    dist2 = dist(tx, ty, df.get_value(row2, "TouchX"), df.get_value(row2, "TouchY"))


    # One is in the target, the other is within 50px from the target
    inside1 = dist1 < targetRadius
    inside2 = dist2 < targetRadius

    #inside1 = df.get_value(row1, "WidgetHovered") == df.get_value(row1, "WidgetHighlighted")
    #inside2 = df.get_value(row2, "WidgetHovered") == df.get_value(row2, "WidgetHighlighted")

    # At least one must be outside of the target
    if (inside1 and inside2):
        return False

    # Both are within 50px of the target
    if (dist1 > targetRadius + 50 or dist2 > targetRadius + 50):
        return False

    return True

def compute(filename):
    if ("fiveFinger" in filename):
        nbFinger = 5
    else:
        nbFinger = 1

    df = pd.read_csv(filename, sep=';')

    df = df[df["TouchEvent"] == "TOUCH_RELEASED"]
    if ("study1" in filename):
        df = df[df["TouchCount"] == nbFinger]

    nbError = 0
    prevRow = 0
    currentTarget = ""
    lastWasError = False

    for idx, row in df.iterrows():
        if ("study2" in filename and df.get_value(idx, "WaitingButtonPress")):
            continue

        target = df.get_value(idx, "WidgetHighlighted")
        if (target == "None"):
            continue

        if (prevRow > 0 and currentTarget != target):
            subject = str(df.get_value(prevRow, "Participant"))
            method = df.get_value(prevRow, "Method")
            brace = "unbraced" if df.get_value(prevRow, "Variant") == "SingleFinger" else "braced"
            vibration = "vibration" if "vibration" in filename else "no_vibration"
            size = str(df.get_value(prevRow, "TargetRadius"))
            countMisses = str(nbError)
            if ("study1" in filename):
                print "\t".join([subject, method, brace, vibration, size, countMisses])
            else:
                print "\t".join([subject, method, vibration, countMisses])
            nbError = 0
            prevRow = 0

        currentTarget = target
        if (prevRow > 0):
            t1 = df.get_value(prevRow, "Timestamp")
            t2 = df.get_value(idx, "Timestamp")
            delta = t2 - t1

            if (not lastWasError and delta <= 500 and is_doubletap_mistake(df, prevRow, idx)):
                nbError = nbError + 1
                lastWasError = True
            else:
                lastWasError = False


        prevRow = idx


def compute_in_dir(dirname):
    for f in os.listdir(dirname):
        if ((len(sys.argv) == 1 or sys.argv[1] in f) and "doubletap" in f):
            compute(dirname + f)


root = "logs/"
for p in os.listdir(root):
    path = root + p + "/"
    compute_in_dir(path)
